<h2>Program</h2>

This software is developed for simulating the dynamics and interaction of individual microtubule elements
in 3D and confined within a closed surface (cell).

<h3>Reference</h3>

Mirabet V, Krupinski P, Hamant O, Meyerowitz EM, Jönsson H, Boudaoud A (2018) 
The self-organization of plant microtubules inside the cell volume yields 
cortical localization, stable alignment, and sensitivity to external cues.
<i>PLoS Comp Biol</i>.

<h3>Author</h3>

vincent.mirabet@gmail.com

<h3>Compilation</h3>

You need various libraries, notably the vtk library, vtk6 or higher. Note, if 
you use VTK>7.1 a preprocessor macro <tt>VTK_VERSION_7_1PLUS</tt> will need to be defined in the preprocessor definitions.
Cmake should be able to detect your VTK version and set definitions automatically.<br>
Using make you can try

<tt>make CPPFLAGS=-DVTK_VERSION_7_1PLUS</tt>

<b>Some examples to get the libraries:</b>

<i>Ubuntu linux (have been tested on 16.04):</i> 

<tt>sudo apt-get install vtk6 libvtk6-dev</tt>  

<i>Mac OS X (have been tested on 10.10-10.12):</i> 

<tt>sudo port install vtk</tt> (using <a href="https://www.macports.org/">macports</a>).

<b>Compiling using cmake:</b>

In the main <tt>microtubule_simulation</tt> directory, do

<tt>cd build</tt><br>
<tt>cmake ..</tt><br>
<tt>make</tt>

This will build a target <tt>programme</tt> in the <tt>build</tt> directory.<br>
To install it into <tt>bin</tt> use

<tt>make install</tt>

<b>Compiling using make:</b>

Modify <tt>src/Makefile.example</tt> to ensure it includes the correct directories for libraries. 

In the main <tt>microtubule_simulation</tt> directory, do

<tt>cd src</tt><br>
<tt>make Makefile.example</tt>

<h3>Binary</h3>

The binary is called <tt>bin/programme</tt>. It is meant to be launched with a 
configuration file as the first (and only) argument:

<tt>bin/programme config.ini</tt>

The <tt>.ini</tt> file contains all the parameter values, and some
example files can be found in the <tt>init</tt> directory.

Usually, <tt>programme</tt> is not called directly, but rather via
bash scripts first generating configuration files (via the python
script <tt>bin/config_generator_13.py</tt>) followed by calls to the
main program. Some example scripts can be found in the <tt>script</tt>
folder. These can be used after the
<i>DIR</i> variables have been updated to point to directories on the
local machine. 

The simplest is the <tt>script/script_test</tt>. It first sets values for model
and simulation parameters, and then calls the python configuration
file generator to generate a <tt>.ini</tt> file followed by calling
the main programme binary for running a simulation. 

The main script used is <tt>script/script_standard</tt>. Inside this
bash script you create a loop where
you define the various parameter values you want to test.  In each step of
the loop it creates a <tt>.ini</tt> file which contains all the parameters
used during the simulation, which ensures that you can recall which
they were.

The <tt>bin/config_generator_13.py</tt> python file contains standard values for all
the parameters, which is convenient if you want to look at what can be
modulated. 

<h3>Outputs</h3>

Microtubule positions are stored as vtk files during the simulation
and the user can specify how often a 'snapshot' should be taken. These
can for example be visualised using <a
href=http://www.paraview.org>Paraview</a> (see example Figure for some
settings that can be used).

<center><img width=500 src="microtubule_simulation_output.png"></center>

Also a file with a summary of the simulation state over time is generated. This little file is
created inside <tt>src/main.cpp</tt> in the <i>rapport()</i> function (if you want to see
or adjust which parameter values that are reported). Again, the user
can specify how often the statistics is reported.

<h3>The c++ code</h3>

Be aware that it is non-optimal as it was my first attempt at coding in c++ language 
with 0 background. 

The basic principle is :
- a <i>MicrotubulePool</i> object manages the microtubules (destruction, creation, evolution...)
- an <i>ElementPool</i> contains all the elements (the tubulin vectors basically and the shell)

Then a Microtubule is an object that contains elements.
The main loop (in main.cpp) launches the evolve function of the MicrotubulePool, 
which in return launches the evolution of each of its microtubules.

The reading/setting of parameter values is made inside the <i>main</i>
function via the class <i>Parametres</i> constructor. All the
functions that concern the growth itself are called grow_something.
